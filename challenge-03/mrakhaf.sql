-- DBMS: PostgreSQL
-- Database: mrakhaf
-- Schema: public (default)
-- Tested on: Jetbrain DataGrip
-- Version: PostgreSQL 14
-- Created by: mrakhaf

-- Semua sudah saya coba dan berhasil semua
-- Silahkan cek folder proof of work untuk bukti bahwa saya berhasil

-- create a database called mrakhaf
drop database if exists mrakhaf;
-- if drop command above failed, use the following command
-- drop database if exists mrakhaf WITH (FORCE);
create database mrakhaf;

-- PLEASE README BEFORE YOU CONTINUE
-- after you create the database, switch to it. (in the terminal, or in the pgAdmin)
-- and then you can run the next queries below

-- create a table called user_game
drop table if exists user_game;
create table user_game (
    id bigserial,
    username character varying(255) not null,
    password character varying(255) not null,
    email varchar(255) not null,
    constraint user_game_pkey primary key (id),
    constraint user_game_username_key unique (username),
    constraint user_game_email_key unique (email)
);

-- create a table called user_game_biodata
drop table if exists user_game_biodata;
create table user_game_biodata (
    id bigserial,
    user_id bigint not null references user_game on delete cascade,
    first_name character varying(255) not null,
    last_name character varying(255) not null,
    age integer not null,
    about text not null,
    constraint user_game_biodata_pkey primary key (id)
);

-- create a table called user_game_history
drop table if exists user_game_history;
create table user_game_history (
    id bigserial,
    user_id bigint not null references user_game on delete cascade,
    game character varying(255) not null,
    score integer not null,
    created_at timestamp with time zone not null,
    constraint user_game_history_pkey primary key (id)
);

-- inserting data into the tables
insert into user_game (username, password, email)
values ('mrakhaf', '123456', 'admin@zecrea.my.id');
insert into user_game (username, password, email)
values ('guest', '123456', 'another@zecrea.my.id');
insert into user_game_biodata (user_id, first_name, last_name, age, about)
values (1, 'M Rakha', 'F', 21, 'I am a web developer');
insert into user_game_biodata (user_id, first_name, last_name, age, about)
values (2, 'Some', 'Guest', 18, 'Just a guest');
insert into user_game_history (user_id, game, score, created_at)
values (1, 'Tic Tac Toe', 100, '2020-01-01 12:00:00');
insert into user_game_history (user_id, game, score, created_at)
values (2, 'Tic Tac Toe', 90, '2020-01-01 11:00:00');

-- select all data from the user_game table
select * from user_game;

-- select all data from the user_game_biodata table
select * from user_game_biodata;

-- select all data from the user_game_history table
select * from user_game_history;

-- select all data from the user_game table and join with user_game_biodata
select 
    user_game.id as user_id,
    user_game.username as username,
    user_game.password as password,
    user_game.email as email,
    user_game_biodata.first_name as first_name,
    user_game_biodata.last_name as last_name,
    user_game_biodata.age as age,
    user_game_biodata.about as about
    from user_game
    join user_game_biodata
    on user_game.id = user_game_biodata.user_id;

-- select all data from the user_game table and join with user_game_history
select 
    user_game.id as user_id,
    user_game.username as username,
    user_game.password as password,
    user_game.email as email,
    user_game_history.game as game,
    user_game_history.score as score,
    user_game_history.created_at as created_at
    from user_game
    join user_game_history
    on user_game.id = user_game_history.user_id;

-- select all data from the user_game table and join with user_game_biodata and user_game_history
select 
    user_game.id as user_id,
    user_game.username as username,
    user_game.password as password,
    user_game.email as email,
    user_game_biodata.first_name as first_name,
    user_game_biodata.last_name as last_name,
    user_game_biodata.age as age,
    user_game_biodata.about as about,
    user_game_history.game as game,
    user_game_history.score as score,
    user_game_history.created_at as created_at
    from user_game
    join user_game_biodata
    on user_game.id = user_game_biodata.user_id
    join user_game_history
    on user_game.id = user_game_history.user_id;

-- update data in the tables
update user_game
set password = '1234567'
where username = 'mrakhaf';
update user_game
set password = '1234567'
where username = 'guest';
update user_game_biodata
set first_name = 'Rakha'
where user_id = 1;

-- delete data from the tables
delete from user_game_history
where user_id = 1;
delete from user_game
where username = 'guest';
delete from user_game
where username = 'mrakhaf';

-- drop tables
drop table if exists user_game_biodata;
drop table if exists user_game_history;
drop table if exists user_game;