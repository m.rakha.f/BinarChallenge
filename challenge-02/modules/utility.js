const readline = require('readline');
const readlineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var getInput = function (message, callback) {
    readlineInterface.question(message, function (answer) {
        callback(answer);
    });
}

var getNumber = function (message, callback) {
    getInput(message, function (answer) {
        var n = parseInt(answer)
        if (isNaN(n)) {
            throw new Error("Not a number");
        }
        
        callback(n);
    });
}

function getInputAsync(message) {
    return new Promise(function (resolve) {
        readlineInterface.question(message, function (answer) {
            resolve(answer);
        });
    });
}

function getNumberAsync(message) {
    return getInputAsync(message).then(function (answer) {
        var n = parseInt(answer)
        if (isNaN(n)) {
            throw new Error("Not a number");
        }

        return n;
    });
}

var closeInputListener = function () {
    readlineInterface.close();
}

module.exports = {
    getNumber,
    getInput,
    getNumberAsync,
    getInputAsync,
    closeInputListener
}