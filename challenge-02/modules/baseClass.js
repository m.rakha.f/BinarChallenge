class Kelas {
    constructor(nama) {
        /** @type {string} */
        // this.__nama;
        /** @type {Array<Murid>} */
        // this.__murid;
        /** @type {Array<MataPelajaran>} */
        // this.__mataPelajaran;

        Object.defineProperties(this, {
            __nama: {
                value: nama,
                writable: true,
                enumerable: false
            },
            __murid: {
                value: [],
                writable: true,
                enumerable: false
            },
            __mataPelajaran: {
                value: [],
                writable: true,
                enumerable: false
            }
        });
    }

    /**
     * Get nama kelas
     * 
     * @return {string}
     */
    get nama() {
        return this.__nama;
    }

    /**
     * Set nama kelas
     * 
     * @param {string} nama
     */
    set nama(nama) {
        if(typeof nama === 'string') {
            this.__nama = nama.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
        }
    }

    /**
     * Get jumlah murid
     * 
     * @return {number}
     */
    get jumlahMurid() {
        return this.__murid.length;
    }

    /**
     * Get murid
     * 
     * @returns {Array}
     */
    get murid() {
        return this.__murid;
    }
    
    /**
     * Set murid
     * @param {Array} murid
     */
    set murid(murid = []) {
        if(
            Array.isArray(murid)
            && murid.every(element => element instanceof Murid)
        ) {
            this.__murid = murid;
        }
    }

    /**
     * Add murid
     * 
     * @param {Murid} murid
     * @param {boolean} chainCall
     * @return {Array<Murid>|boolean}
     */
    addMurid(murid, chainCall = false) {
        //check if murid instance of siswa class
        if(murid instanceof Murid) {
            this.__murid.push(murid);

            if(!chainCall) {
                murid.addKelas(this, true);
            }

            return this.__murid;
        }

        return false;
    }

    /**
     * Get jumlah matapelajaran
     * 
     * @return {number}
     */
    get jumlahMataPelajaran() {
        return this.__mataPelajaran.length;
    }

    /**
     * Get matapelajaran
     * 
     * @returns {Array<MataPelajaran>}
     */
    get mataPelajaran() {
        return this.__mataPelajaran;
    }

    /**
     * Set matapelajaran
     * 
     * @param {Array<MataPelajaran>} mataPelajaran
     */
    set mataPelajaran(mataPelajaran = []) {
        if(
            Array.isArray(mataPelajaran) 
            && mataPelajaran.every(element => element instanceof MataPelajaran)
        ) {
            this.__mataPelajaran = mataPelajaran;
        }
    }

    /**
     * Add mata pelajaran
     * 
     * @param {MataPelajaran} mataPelajaran
     * @param {boolean} chainCall
     * @return {Array<MataPelajaran>|boolean}
     */
    addMataPelajaran(mataPelajaran, chainCall = false) {
        if(mataPelajaran instanceof MataPelajaran) {
            this.__mataPelajaran.push(mataPelajaran);

            if(!chainCall) {
                mataPelajaran.addKelas(this, true);
            }

            return this.__mataPelajaran;
        }

        return false;
    }

    /**
     * Check if kelas has murid
     * 
     * @param {Murid} murid
     * @return {boolean}
     */
    hasMurid(murid) {
        return this.__murid.includes(murid);
    }

    /**
     * Check if kelas has mata pelajaran
     * 
     * @param {MataPelajaran} mataPelajaran
     * @return {boolean}
     */
    hasMataPelajaran(mataPelajaran) {
        return this.__mataPelajaran.includes(mataPelajaran);
    }

    /**
     * Check if kelas has murid and mata pelajaran
     * 
     * @param {Murid} murid
     * @param {MataPelajaran} mataPelajaran
     * @return {boolean}
     */
    hasMataPelajaranAndMurid(mataPelajaran, murid) {
        return this.hasMataPelajaran(mataPelajaran) && this.hasMurid(murid);
    }

    /**
     * Check if kelas has murid or mata pelajaran
     * 
     * @param {Murid} murid
     * @param {MataPelajaran} mataPelajaran
     * @return {boolean}
     */
    hasMataPelajaranOrMurid(mataPelajaran, murid) {
        return this.hasMataPelajaran(mataPelajaran) || this.hasMurid(murid);
    }

    /**
     * Where is mata pelajaran index
     * 
     * @param {MataPelajaran} mataPelajaran 
     * @returns 
     */
    whereIsMataPelajaran(mataPelajaran) {
        return this.__mataPelajaran.indexOf(mataPelajaran);
    }

    /**
     * Where is murid index
     * 
     * @param {Murid} murid 
     * @returns 
     */
    whereIsMurid(murid) {
        return this.__murid.indexOf(murid);
    }
}

/** 
 * @typedef {Object} Nilai
 * @property {Murid} owner
 * @property {number} value
 */

class MataPelajaran {
    constructor(nama, kkm = 75) {
        /** @type {string} */
        // this.__nama;
        /** @type {number} */
        // this.__kkm;
        /** @type {Array<Murid>} */
        // this.__murid;
        /** @type {Kelas} */
        // this.__kelas;

        /** @type {Array<Nilai>} */
        // this.__nilai;

        Object.defineProperties(this, {
            __nama: {
                value: nama,
                writable: true,
                enumerable: false
            },
            __kkm: {
                value: kkm || 75,
                writable: true,
                enumerable: false
            },
            __murid: {
                value: [],
                writable: true,
                enumerable: false
            },
            __kelas: {
                value: [],
                writable: true,
                enumerable: false
            },
            __nilai: {
                value: [],
                writable: true,
                enumerable: false
            }
        });
    }

    /**
     * Get nama
     * 
     * @return {string}
     */
    get nama() {
        return this.__nama;
    }

    /**
     * Set nama
     * 
     * @param {string} nama
     */
    set nama(nama) {
        if(typeof nama === 'string') {
            this.__nama = nama.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
        }
    }


    /**
     * Get kkm
     * 
     * @return {number}
     */
    get kkm() {
        return this.__kkm;
    }

    /**
     * Set kkm
     * 
     * @param {number} kkm
     */
    set kkm(value) {
        if (typeof value === 'number') {
            this.__kkm = value;
        }
    }

    /**
     * Get peserta
     * 
     * @return {Array<Murid>}
     */
    get peserta() {
        return this.__murid;
    }

    /**
     * Set peserta
     * 
     * @param {Array<Murid>} peserta
     */
    set peserta(value) {
        if (
            Array.isArray(value)
            && value.every(element => element instanceof Murid)
        ) {
            this.__murid = value;
        }
    }

    /**
     * Add peserta
     * 
     * @param {Murid} murid
     * @param {boolean} chainCall
     * @return {Array<Murid>|boolean}
     */
    addPeserta(murid, chainCall = false) {
        if(murid instanceof Murid) {
            this.__murid.push(murid);

            if(!chainCall) {
                murid.addMataPelajaran(this, true);
            }

            return this.__murid;
        }

        return false;
    }

    /**
     * Get kelas
     * 
     * @return {Array<Kelas>}
     */
    get kelas() {
        return this.__kelas;
    }

    /**
     * Set kelas
     * 
     * @param {Array<Kelas>} kelas
     */
    set kelas(value) {
        //check is array and every element is instance of kelas class
        if(
            Array.isArray(value) 
            && value.every(element => element instanceof Kelas)
        ) {
            this.__kelas = value;
        }
    }

    /**
     * Add kelas
     * 
     * @param {Kelas} kelas
     * @param {boolean} chainCall
     * @return {Array<Kelas>|boolean}
     */
    addKelas(kelas, chainCall = false) {
        if(kelas instanceof Kelas) {
            this.__kelas.push(kelas);

            if(!chainCall) {
                kelas.addMataPelajaran(this, true);
            }

            return this.__kelas;
        }

        return false;
    }

    /**
     * Add nilai
     * 
     * @param {Murid} murid
     * @param {number} nilai
     */
    addNilai(murid, nilai) {
        if(
            murid instanceof Murid 
            && typeof nilai === 'number'
        ) {
            this.__nilai.push({
                owner: murid,
                value: nilai
            });
        }
    }


    /**
     * Get nilai
     * 
     * @return {Array<Nilai>}
     */
    get nilai(){
        return this.__nilai;
    }

    set nilai(value){
        //
    }

    /**
     * Get nilai
     * 
     * @param {Murid} murid
     * @param {boolean} returnObject
     * @return {Nilai|number}
     */
    getNilaiOf(murid, returnObject = false) {
        if(murid instanceof Murid) {
            if(returnObject) {
                return this.__nilai.find(element => element.owner === murid);
            }

            return this.__nilai.find(element => element.owner === murid).value;
        }
    }

    sortNilai() {
        this.__nilai.sort((a, b) => {
            if(a.value > b.value) {
                return -1;
            } else if(a.value < b.value) {
                return 1;
            }

            return 0;
        });
    }

    isHighest(murid) {
        if(murid instanceof Murid) {
            return this.__nilai.find(element => element.owner === murid).value === this.__nilai[this.__nilai.length - 1].value;
        }
        
        return false;
    }

    isLowest(murid) {
        if(murid instanceof Murid) {
            return this.__nilai.find(element => element.owner === murid).value === this.__nilai[0].value;
        }

        return false;
    }

    isPassed(murid) {
        if(murid instanceof Murid) {
            return this.__nilai.find(element => element.owner === murid).value >= this.__kkm;
        }

        return false;
    }

    isFailed(murid) {
        if(murid instanceof Murid) {
            return this.__nilai.find(element => element.owner === murid).value < this.__kkm;
        }

        return false;
    }

    /**
     * Check if mata pelajaran has kelas
     * 
     * @param {Kelas} kelas
     * @return {boolean}
     */
    hasKelas(kelas) {
        return this.__kelas.includes(kelas);
    }

    /**
     * Check if mata pelajaran has peserta
     * 
     * @param {Murid} murid
     * @return {boolean}
     */
    hasPeserta(murid) {
        return this.__murid.includes(murid);
    }

    /**
     * Check if mata pelajaran has peserta and kelas
     * 
     * @param {Murid} murid
     * @param {Kelas} kelas
     * @return {boolean}
     */
    hasKelasAndPeserta(kelas, murid) {
        return this.hasKelas(kelas) && this.hasPeserta(murid);
    }

    /**
     * Check if mata pelajaran has kelas or peserta
     * 
     * @param {Kelas} kelas
     * @param {Murid} murid
     * @return {boolean}
     */
    hasKelasOrPeserta(kelas, murid) {
        return this.hasKelas(kelas) || this.hasPeserta(murid);
    }

    /**
     * Where kelas index
     * 
     * @param {Kelas} kelas
     * @return {number}
     */
    whereIsKelas(kelas) {
        return this.__kelas.indexOf(kelas);
    }

    /**
     * Where peserta index
     * 
     * @param {Murid} murid
     * @return {number}
     */
    whereIsPeserta(murid) {
        return this.__murid.indexOf(murid);
    }
}

class Murid {
    constructor(nama) {
        /** @type {string} */
        // this.__nama;
        /** @type {Array<Kelas>} */
        // this.__kelas;
        /** @type {Array<MataPelajaran>} */
        // this.__mataPelajaran;

        Object.defineProperties(this, {
            __nama: {
                value: nama,
                writable: true,
                enumerable: false
            },
            __kelas: {
                value: [],
                writable: true,
                enumerable: false
            },
            __mataPelajaran: {
                value: [],
                writable: true,
                enumerable: false
            }
        });
    }

    /**
     * Get nama
     * 
     * @return {string}
     */
    get nama() {
        return this.__nama;
    }

    /**
     * Set nama
     * 
     * @param {string} nama
     */
    set nama(nama) {
        if(typeof nama === 'string') {
            this.__nama = nama.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
        }
    }

    /**
     * Get kelas
     * 
     * @return {Array<Kelas>}
     */
    get kelas() {
        return this.__kelas;
    }

    /**
     * Set kelas
     * 
     * @param {Array<Kelas>} kelas
     */
    set kelas(value) {
        if(
            Array.isArray(value) 
            && value.every(element => element instanceof Kelas)
        ) {
            this.__kelas = value;
        }
    }

    /**
     * Add kelas
     * 
     * @param {Kelas} kelas
     * @param {boolean} chainCall
     * @return {Array<Kelas>|boolean}
     */
    addKelas(kelas, chainCall = false) {
        if(
            kelas instanceof Kelas
            && this.hasKelas(kelas) === false
        ) {
            this.__kelas.push(kelas);

            if(!chainCall) {
                kelas.addMurid(this, true);
            }

            return this.__kelas;
        }

        return false;
    }

    /**
     * Get mata pelajaran
     * 
     * @return {Array<MataPelajaran>}
     */
    get mataPelajaran() {
        return this.__mataPelajaran;
    }

    /**
     * Set mata pelajaran
     * 
     * @param {Array<MataPelajaran>} mataPelajaran
     */
    set mataPelajaran(value) {
        if(
            Array.isArray(value) 
            && value.every(element => element instanceof MataPelajaran)
        ) {
            this.__mataPelajaran = value;
        }
    }

    /**
     * Add mata pelajaran
     * 
     * @param {MataPelajaran} mataPelajaran
     * @param {boolean} chainCall
     * @return {Array<MataPelajaran>|boolean}
     */
    addMataPelajaran(mataPelajaran, chainCall = false) {
        if(
            mataPelajaran instanceof MataPelajaran
            && this.hasMataPelajaran(mataPelajaran) === false
        ) {
            this.__mataPelajaran.push(mataPelajaran);

            if(!chainCall) {
                mataPelajaran.addPeserta(this, true);
            }
            
            return this.__mataPelajaran;
        }

        return false;
    }

    /**
     * Check if murid has kelas
     * 
     * @param {Kelas} kelas
     * @return {boolean}
     */
    hasKelas(kelas) {
        return this.__kelas.includes(kelas);
    }

    /**
     * Check if murid has mata pelajaran
     * 
     * @param {MataPelajaran} mataPelajaran 
     * @return {boolean}
     */
    hasMataPelajaran(mataPelajaran) {
        return this.__mataPelajaran.includes(mataPelajaran);
    }

    /**
     * Check if murid has kelas and mata pelajaran
     * 
     * @param {Kelas} kelas
     * @param {MataPelajaran} mataPelajaran
     * @return {boolean}
     */
    hasKelasAndMataPelajaran(kelas, mataPelajaran) {
        return this.hasKelas(kelas) && this.hasMataPelajaran(mataPelajaran);
    }

    /**
     * Check if murid has kelas or mata pelajaran
     * 
     * @param {Kelas} kelas
     * @param {MataPelajaran} mataPelajaran
     * @return {boolean}
     */
    hasKelasOrMataPelajaran(kelas, mataPelajaran) {
        return this.hasKelas(kelas) || this.hasMataPelajaran(mataPelajaran);
    }

    /**
     * Get kelas index
     * 
     * @param {Kelas} kelas
     * @return {number}
     */
    whereIsKelas(kelas) {
        return this.__kelas.indexOf(kelas);
    }

    /**
     * Get mata pelajaran index
     * 
     * @param {MataPelajaran} mataPelajaran 
     * @return {number}
     */
    whereIsMataPelajaran(mataPelajaran) {
        return this.__mataPelajaran.indexOf(mataPelajaran);
    }
}

module.exports = {
    Murid,
    Kelas,
    MataPelajaran
}