const baseClass = require(__dirname+'/./baseClass.js');

module.exports = {
    Kelas: baseClass.Kelas,
    Murid: baseClass.Murid,
    MataPelajaran: baseClass.MataPelajaran,
    utility: require(__dirname+'/./utility.js'),
}