const modules = require(__dirname+'/./modules');

//simple example of usage this repository
/** @type {import('./modules/baseClass').Kelas} */
const kelas = new modules.Kelas('Kelas 4');
/** @type {import('./modules/baseClass').MataPelajaran} */
const mataPelajaran = new modules.MataPelajaran('Backend JavaScript');
/** @type {Array<import('./modules/baseClass').Murid>} */
const murid = [];

const getInput = modules.utility.getInputAsync;
const getNumber = modules.utility.getNumberAsync;
const closeInput = modules.utility.closeInputListener;

kelas.addMataPelajaran(mataPelajaran); // no need to ad kelas on mataPelajaran

console.log("=====[SIMPLE EXAMPLE OF USAGE]=====");
console.log("Kelas: " + kelas.nama);
console.log("Mata pelajaran: " + mataPelajaran.nama);

async function main(){
    var input = await getInput("Masukkan nama murid: ");
    /** @type {import('./modules/baseClass').Murid} */
    var muridTmp = new modules.Murid(input);

    muridTmp.addMataPelajaran(mataPelajaran);
    muridTmp.addKelas(kelas);
    murid.push(muridTmp);

    var nilai = await getNumber("Masukan nilai murid: ");
    mataPelajaran.addNilai(muridTmp, nilai);
            
    var inputQ = await getInput("Tambahkan lagi? (y/q): ");

    if(inputQ.toLowerCase() === 'y'){
        main();
    }else if(inputQ.toLowerCase() === 'q'){
        console.clear();
        console.log("=====[RESULT]=====");
        console.log("Kelas: " + kelas.nama);
        console.log("Mata pelajaran: " + mataPelajaran.nama);
        console.log("Murid: ");
        mataPelajaran.sortNilai();

        mataPelajaran.nilai.forEach(function(item, index){
            const highestOrLowest = index === 0 ? 
                'highest' 
                : 
                index === mataPelajaran.nilai.length - 1 ?
                    'lowest'
                    :
                    'middle';
            const passedOrFailed = mataPelajaran.isPassed(item.owner) ? 'passed' : 'failed';
            let printText = "\t" + item.owner.nama + ": " + item.value + " (" + highestOrLowest + ")" + " (" + passedOrFailed + ")";
            console.log(printText);
        });

        closeInput();
        return;
    }
}

main();