// Language: javascript
// Runtime: Node.js
// Application: calculator basic

const readline = require('readline');
const readlineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var menu = "\
1. Addition\n\
2. Subtraction\n\
3. Multiplication\n\
4. Division\n\
5. Square Root\n\
6. Area of Square\n\
7. Volume of a Cube\n\
8. Volume of a Tube\n\
9. Quit";

var border = "=================";

var choice = 0;

// utility func
function getInput(message, callback) {
    readlineInterface.question(message, function (answer) {
        callback(answer);
    });
}

var getNumber = function (message, callback) {
    getInput(message, function (answer) {
        var n = parseInt(answer)
        if (isNaN(n)) {
            throw new Error("Not a number");
        }
        
        callback(n);
    });
}

var pressEnterToContinue = function (callback) {
    console.log("Press Enter to continue...");
    readlineInterface.question("", function (answer) {
        callback();
    });
}

function addition(n1, n2){
    return n1 + n2;
}

function substraction(n1, n2){
    return n1 - n2;
}

function multiplication(n1, n2){
    return n1 * n2;
}

function division(n1, n2){
    return n1 / n2;
}

function squareRoot(n1){
    return Math.sqrt(n1);
}

function areaOfSquare(n1){
    return n1 * n1;
}

function volumeOfCube(n1){
    return n1 * n1 * n1;
}

function volumeOfTube(r, t){
    return Math.PI * r * r * t;
}

// main func
function additionMenu(){
    getNumber("Enter first number: ", function (n1) {
        getNumber("Enter second number: ", function (n2) {
            console.log("Result: " + addition(n1, n2));
            pressEnterToContinue(main);
        });
    });
}

function substractionMenu(){
    getNumber("Enter first number: ", function (n1) {
        getNumber("Enter second number: ", function (n2) {
            console.log("Result: " + substraction(n1, n2));
            pressEnterToContinue(main);
        });
    });
}

function multiplicationMenu(){
    getNumber("Enter first number: ", function (n1) {
        getNumber("Enter second number: ", function (n2) {
            console.log("Result: " + multiplication(n1, n2));
            pressEnterToContinue(main);
        });
    });
}

function divisionMenu(){
    getNumber("Enter first number: ", function (n1) {
        getNumber("Enter second number: ", function (n2) {
            console.log("Result: " + division(n1, n2));
            pressEnterToContinue(main);
        });
    });
}

function squareRootMenu(){
    getNumber("Enter number: ", function (n1) {
        console.log("Result: " + squareRoot(n1));
        pressEnterToContinue(main);
    });
}

function areaOfSquareMenu(){
    getNumber("Enter number: ", function (n1) {
        console.log("Result: " + areaOfSquare(n1));
        pressEnterToContinue(main);
    });
}

function volumeOfCubeMenu(){
    getNumber("Enter number: ", function (n1) {
        console.log("Result: " + volumeOfCube(n1));
        pressEnterToContinue(main);
    });
}

function volumeOfTubeMenu(){
    getNumber("Enter radius: ", function (r) {
        getNumber("Enter height: ", function (t) {
            console.log("Result: " + volumeOfTube(r, t));
            pressEnterToContinue(main);
        });
    });
}

function main(){
    console.clear();
    console.log(border);
    console.log(menu);
    console.log(border);
    getNumber("Enter your choice: ", function (answer) {
        choice = answer;
        switch(choice){
            case 1:
                console.clear();
                additionMenu();
            break;
            case 2:
                console.clear();
                substractionMenu();
            break;
            case 3:
                console.clear();
                multiplicationMenu();
            break;
            case 4:
                console.clear();
                divisionMenu();
            break;
            case 5:
                console.clear();
                squareRootMenu();
            break;
            case 6:
                console.clear();
                areaOfSquareMenu();
            break;
            case 7:
                console.clear();
                volumeOfCubeMenu();
            break;
            case 8:
                console.clear();
                volumeOfTubeMenu();
            break;
            case 9:
                console.log("Bye!");
                readlineInterface.close();
            break;
            default:
                console.log("Invalid choice!");
                main();
            break;
        }
    });
}

main();