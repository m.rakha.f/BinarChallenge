// Language: javascript
// Runtime: Node.js
// Application: calculator basic

const readline = require('readline');
const readlineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var menu = "\
1. Addition\n\
2. Subtraction\n\
3. Multiplication\n\
4. Division\n\
5. Square Root\n\
6. Area of Square\n\
7. Volume of a Cube\n\
8. Volume of a Tube\n\
9. Quit";

var border = "=================";

var choice = 0;

// utility func
// function getInput(message, callback) {
//     readlineInterface.question(message, function (answer) {
//         callback(answer);
//     });
// }

// var getNumber = function (message, callback) {
//     getInput(message, function (answer) {
//         var n = parseInt(answer)
//         if (isNaN(n)) {
//             throw new Error("Not a number");
//         }
        
//         callback(n);
//     });
// }

// var pressEnterToContinue = function (callback) {
//     console.log("Press Enter to continue...");
//     readlineInterface.question("", function (answer) {
//         callback();
//     });
// }

function getInputAsync(message) {
    return new Promise(function (resolve) {
        readlineInterface.question(message, function (answer) {
            resolve(answer);
        });
    });
}

function getNumberAsync(message) {
    return getInputAsync(message).then(function (answer) {
        var n = parseInt(answer)
        if (isNaN(n)) {
            throw new Error("Not a number");
        }

        return n;
    });
}

function pressEnterToContinueAsync () {
    return new Promise(function (resolve) {
        console.log("Press Enter to continue...");
        readlineInterface.question("", function (answer) {
            resolve();
        });
    });
}

function addition(n1, n2){
    return n1 + n2;
}

function substraction(n1, n2){
    return n1 - n2;
}

function multiplication(n1, n2){
    return n1 * n2;
}

function division(n1, n2){
    return n1 / n2;
}

function squareRoot(n1){
    return Math.sqrt(n1);
}

function areaOfSquare(n1){
    return n1 * n1;
}

function volumeOfCube(n1){
    return n1 * n1 * n1;
}

function volumeOfTube(r, t){
    return Math.PI * r * r * t;
}

// main func
function additionMenu(){
    return getNumberAsync("Enter first number: ").then(function (n1) {
        return getNumberAsync("Enter second number: ").then(async function (n2) {
            console.log("Result: " + addition(n1, n2));
            await pressEnterToContinueAsync();
        });
    });
}

function substractionMenu(){
    return getNumberAsync("Enter first number: ").then(function (n1) {
        return getNumberAsync("Enter second number: ").then(async function (n2) {
            console.log("Result: " + substraction(n1, n2));
            await pressEnterToContinueAsync();
        });
    });
}

function multiplicationMenu(){
    return getNumberAsync("Enter first number: ").then(function (n1) {
        return getNumberAsync("Enter second number: ").then(async function (n2) {
            console.log("Result: " + multiplication(n1, n2));
            await pressEnterToContinueAsync();
        });
    });
}

function divisionMenu(){
    return getNumberAsync("Enter first number: ").then(function (n1) {
        return getNumberAsync("Enter second number: ").then(async function (n2) {
            console.log("Result: " + division(n1, n2));
            await pressEnterToContinueAsync();
        });
    });
}

function squareRootMenu(){
    return getNumberAsync("Enter number: ").then(async function (n1) {
        console.log("Result: " + squareRoot(n1));
        await pressEnterToContinueAsync();
    });
}

function areaOfSquareMenu(){
    return getNumberAsync("Enter number: ").then(async function (n1) {
        console.log("Result: " + areaOfSquare(n1));
        await pressEnterToContinueAsync();
    });
}

function volumeOfCubeMenu(){
    return getNumberAsync("Enter number: ").then(async function (n1) {
        console.log("Result: " + volumeOfCube(n1));
        await pressEnterToContinueAsync();
    });
}

function volumeOfTubeMenu(){
    return getInputAsync("Enter radius: ").then(function (r) {
        return getInputAsync("Enter thickness: ").then(async function (t) {
            console.log("Result: " + volumeOfTube(r, t));
            await pressEnterToContinueAsync();
        });
    });
}

async function main(){
    do {
        console.clear();
        console.log(border);
        console.log(menu);
        console.log(border);
        choice = await getNumberAsync("Enter your choice: ");
        switch(choice){
            case 1:
                await additionMenu();
            break;
            case 2:
                await substractionMenu();
            break;
            case 3:
                await multiplicationMenu();
            break;
            case 4:
                await divisionMenu();
            break;
            case 5:
                await squareRootMenu();
            break;
            case 6:
                await areaOfSquareMenu();
            break;
            case 7:
                await volumeOfCubeMenu();
            break;
            case 8:
                await volumeOfTubeMenu();
            break;
            case 9:
                console.log("Bye!");
                readlineInterface.close();
            break;
            default:
                console.log("Invalid choice!");
                main();
            break;
        }
    } while (choice != 9);
}

main();