const { PrismaClient } = require('@prisma/client')


const prisma = new PrismaClient()

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(
    !req.body.user_id
    || !req.body.first_name
    || !req.body.last_name
    || !req.body.age
    || !req.body.about
  ){
    return res.json({
      error: true,
      message: 'User id, first name, last name, age and about are required',
      data: [],
    })
  }

  const data = await prisma.user_game_biodata.create({
    data: {
      user_game: {
        connect: {
          id: parseInt(req.body.user_id)
        }
      },
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      age: parseInt(req.body.age),
      about: req.body.about,
    }
  }).catch(err => {
    return res.json({
      error: true,
      message: err.message,
      data: [],
    })
  });

  res.json({
    error: false,
    message: 'User game biodata created',
    data: [data],
  })
}

module.exports = controller;
