const { PrismaClient } = require('@prisma/client')


const prisma = new PrismaClient()

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  var id = parseInt(req.params.id);

  if(isNaN(id)) {
    return res.json({
      error: true,
      message: 'Invalid user id',
      data: [],
    })
  }

  const data = await prisma.user_game.findFirst({
    where: {
      id
    }
  }).catch(err => {
    return res.json({
      error: true,
      message: err.message,
      data: [],
    })
  });

  if(!data) {
    return res.json({
      error: true,
      message: 'User not found',
      data: [],
    })
  }

  res.json({
    error: false,
    message: 'Success',
    data: [data],
  })
}

module.exports = controller;
