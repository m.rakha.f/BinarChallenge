const { PrismaClient } = require('@prisma/client')


const prisma = new PrismaClient()

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  const data = await prisma.user_game.findMany().catch(err => {
    return res.json({
      error: true,
      message: err.message,
      data: [],
    })
  });

  if(!data) {
    return res.json({
      error: true,
      message: 'No user found',
      data: [],
    })
  }

  res.json({
    error: false,
    message: 'Success',
    data: [data],
  })
}

module.exports = controller;
