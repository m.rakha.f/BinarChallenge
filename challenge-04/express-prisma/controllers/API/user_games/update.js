const { PrismaClient } = require('@prisma/client')


const prisma = new PrismaClient()

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.body.username || !req.body.password || !req.body.email) {
    return res.json({
      error: true,
      message: 'Username, password and email are required',
      data: [],
    })
  }

  var id = parseInt(req.params.id);

  if(isNaN(id)) {
    return res.json({
      error: true,
      message: 'Invalid user id',
      data: [],
    })
  }

  const update = await prisma.user_game.update({
    where: {
      id
    },
    data: {
      username: req.body.username,
      password: req.body.password,
      email: req.body.email
    }
  }).catch(err => {
    return res.json({
      error: true,
      message: err.message,
      data: [],
    })
  });

  res.json({
    error: false,
    message: 'User updated',
    data: [update],
  })
}

module.exports = controller;
