const { PrismaClient } = require('@prisma/client')


const prisma = new PrismaClient()

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.cookies.loggedIn) {
    return res.redirect('/');
  }

  const user_games = await prisma.user_game.findMany();

  res.render('layouts/default', {
    title: 'HOME | MRAKHAF',
    ejsInclude: {
      body: process.env.EXPRESS_ROOT+'/views/pages/home',
    },
    ejsParameter: {
      body: '',
      user_games
    }
  });
}

module.exports = controller;
