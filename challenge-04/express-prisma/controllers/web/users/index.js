module.exports = {
  view: require('./view'),
  create: require('./create'),
  edit: require('./edit')
}
