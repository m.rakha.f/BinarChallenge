const { PrismaClient } = require('@prisma/client')


const prisma = new PrismaClient()

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.cookies.loggedIn) {
    return res.redirect('/');
  }

  var id = parseInt(req.params.id);

  if(isNaN(id)) {
    return res.redirect('/');
  }

  const user_game = await prisma.user_game.findFirst({
    where: {
      id: id
    }
  })

  if(!user_game) {
    return res.redirect('/');
  }

  res.render('layouts/default', {
    title: 'USER_GAME | MRAKHAF',
    ejsInclude: {
      body: process.env.EXPRESS_ROOT+'/views/pages/users/view',
    },
    ejsParameter: {
      body: '',
      user_game
    }
  });
}

module.exports = controller;
