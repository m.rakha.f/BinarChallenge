/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
function controller(req, res, next) {
  if(req.cookies.loggedIn) {
    return res.redirect('/home');
  }

  res.render('layouts/default', {
    title: 'Login | MRAKHAF',
    ejsInclude: {
      body: process.env.EXPRESS_ROOT+'/views/pages/login',
    },
    ejsParameter: {
      body: ''
    }
  });
}

module.exports = controller;
