var express = require('express');
var router = express.Router();
var rootDir = process.env.EXPRESS_ROOT;
var rootController = rootDir+'/controllers/api/';

var loginController = require(rootController+'login');
var userController = require(rootController+'user_games');
var userGameBiodataController = require(rootController+'user_game_biodatas');
var userGameHistoriesController = require(rootController+'user_game_histories');

router.post('/login', loginController);

router.get('/user_games', userController.all);
router.get('/user_games/:id', userController.view);
router.post('/user_games', userController.store);
router.put('/user_games/:id', userController.update);
router.patch('/user_games/:id', userController.update);
router.delete('/user_games/:id', userController.destroy);

router.get('/user_game_biodatas', userGameBiodataController.all);
router.get('/user_game_biodatas/:id', userGameBiodataController.view);
router.post('/user_game_biodatas', userGameBiodataController.store);
router.put('/user_game_biodatas/:id', userGameBiodataController.update);
router.patch('/user_game_biodatas/:id', userGameBiodataController.update);
router.delete('/user_game_biodatas/:id', userGameBiodataController.destroy);

router.get('/user_game_histories', userGameHistoriesController.all);
router.get('/user_game_histories/:id', userGameHistoriesController.view);
router.post('/user_game_histories', userGameHistoriesController.store);
router.put('/user_game_histories/:id', userGameHistoriesController.update);
router.patch('/user_game_histories/:id', userGameHistoriesController.update);
router.delete('/user_game_histories/:id', userGameHistoriesController.destroy);

router.all('/*', function(req, res, next) {
  res.json({
    error: true,
    message: 'Not found',
    data: [],
  })
});

module.exports = router;
